module Main where

import           Data.Yaml                      ( decodeFileEither )
import           Sockets.Server                 ( runServer )

import           Options.Applicative            ( Parser
                                                , ParserInfo
                                                , execParser
                                                , fullDesc
                                                , header
                                                , help
                                                , helper
                                                , info
                                                , long
                                                , metavar
                                                , progDesc
                                                , short
                                                , strOption
                                                )


newtype CmdOpts = CmdOpts
  { configPath :: String
  } deriving (Show)

main :: IO ()
main = do
  cmdOpts    <- execParser parserInfo
  eitherOpts <- decodeFileEither (configPath cmdOpts)
  case eitherOpts of
    Right opts -> runServer opts
    Left  e    -> error $ show e

parserInfo :: ParserInfo CmdOpts
parserInfo = info
  (helper <*> optionParser)
  (fullDesc <> header "chatserver" <> progDesc
    "A chat server with decent language :)"
  )

optionParser :: Parser CmdOpts
optionParser = CmdOpts <$> strOption
  (long "config-path" <> short 'c' <> metavar "CONFIGPATH" <> help
    "Path to config-file"
  )
