module Main where

import           Options.Applicative            ( Parser
                                                , ParserInfo
                                                , auto
                                                , execParser
                                                , fullDesc
                                                , header
                                                , help
                                                , helper
                                                , info
                                                , long
                                                , metavar
                                                , option
                                                , progDesc
                                                , short
                                                , strOption
                                                )
import           Sockets.Client                 ( ClientOpts(..)
                                                , runClient
                                                )


main :: IO ()
main = execParser parserInfo >>= runClient

parserInfo :: ParserInfo ClientOpts
parserInfo = info
  (helper <*> optionParser)
  (fullDesc <> header "chatclient" <> progDesc "A chat with decent language :)")

optionParser :: Parser ClientOpts
optionParser =
  ClientOpts
    <$> strOption
          (long "server-addr" <> short 'a' <> metavar "SERVERADDR" <> help
            "chat server's address"
          )
    <*> option
          auto
          (long "server-port" <> short 'p' <> metavar "SERVERPORT" <> help
            "port the chat server is running on"
          )
