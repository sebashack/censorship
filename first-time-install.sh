#!/bin/bash

set -xeuf -o pipefail

export TOP_LEVEL="$( readlink -f "$( dirname "${BASH_SOURCE[0]}" )" )"

if [[ -v CI ]]; then
   SUDO=""
else
   SUDO="sudo"
fi

# Install system deps
${SUDO} apt-get update
${SUDO} apt-get install -y build-essential icu-devtools libicu-dev libtinfo-dev

# Install python deps
${SUDO} apt-get install -y tar python3-venv python3-pip python3-tk

# Install Haskell stack
if ! type "stack" > /dev/null; then
    curl -sSL https://get.haskellstack.org/ | sh
else
    echo 'stack already installed'
fi
