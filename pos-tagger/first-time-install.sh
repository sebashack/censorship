#!/bin/bash

set -xeuf -o pipefail

pip3 install black
pip3 install flake8
