from spacy import displacy
import spacy


def load_model():
    return spacy.load("es_core_news_md")


def tag_spanish_text(nlp, txt):
    tokens = []
    for token in nlp(txt):
        props = {
            "word": token.text,
            "lemma": token.lemma_,
            "lexeme": token.lex.norm_,
            "pos": token.pos_,
            "isPunct": token.is_punct,
            "dependency": token.dep_,
        }
        tokens.append(props)

    return tokens


def visualize_dependencies(nlp, txt):
    doc = nlp(txt)
    displacy.serve(doc, style="dep")
