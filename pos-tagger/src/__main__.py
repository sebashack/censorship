import sys
import argparse
import json

from tagger import load_model, tag_spanish_text, visualize_dependencies


def main(argv):
    parser = argparse.ArgumentParser(description="Spanish Tagger")
    parser.add_argument(
        "-r",
        "--raw-text",
        required=True,
        metavar="INPUT",
        type=str,
        help="tagger input",
    )
    parser.add_argument(
        "-v",
        "--visual-mode",
        action="store_true",
        help="visualize sentence dependencies",
    )
    args = parser.parse_args()

    input_text = args.raw_text
    visualize = args.visual_mode

    model = load_model()

    if visualize:
        visualize_dependencies(model, input_text)
    else:
        print(json.dumps(tag_spanish_text(model, input_text)))


if __name__ == "__main__":
    main(sys.argv[1:])
