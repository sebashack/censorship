{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric              #-}

module Sockets.Server where

import           Control.Concurrent             ( MVar
                                                , modifyMVar
                                                , modifyMVar_
                                                , newMVar
                                                , readMVar
                                                )
import           Control.Exception              ( finally )
import           Control.Monad                  ( forM_
                                                , forever
                                                )
import           Data.Aeson                     ( FromJSON(..) )
import           Data.Char                      ( isPunctuation
                                                , isSpace
                                                )
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import qualified Data.Text.IO                  as TIO
import           Data.Word                      ( Word16 )
import           GHC.Generics
import qualified Network.WebSockets            as WS
import           System.Directory               ( doesDirectoryExist
                                                , doesFileExist
                                                )
import           System.FilePath.Posix          ( (</>) )

import           Nlp.Analyzer                   ( Analyzer
                                                , mkAnalyzer
                                                )


data ServerOpts = ServerOpts
  { port            :: Word16
  , host            :: String
  , taggerDir       :: FilePath
  , prohibitedWords :: FilePath
  , badWords        :: FilePath
  , contextualWords :: FilePath
  }
  deriving (Generic, Show)

instance FromJSON ServerOpts

type Client = (Text, WS.Connection)
type ServerState = [Client]

runServer :: ServerOpts -> IO ()
runServer opts = do
  pWords <- readWords (prohibitedWords opts)
  bWords <- readWords (badWords opts)
  cWords <- readWords (contextualWords opts)
  let taggerPath = taggerDir opts </> "src"
      pythonBin  = taggerDir opts </> ".env" </> "bin" </> "python"
  checkPyDeps pythonBin taggerPath
  let analyzer :: Analyzer
      analyzer = mkAnalyzer pythonBin taggerPath pWords bWords cWords
  state <- newMVar newServerState
  putStrLn $ "Running server on " <> host opts <> ":" <> (show $ port opts)
  WS.runServer (host opts) (fromIntegral $ port opts)
    $ application analyzer state
 where
  checkPyDeps :: FilePath -> FilePath -> IO ()
  checkPyDeps pyBinPath taggerDirPath = do
    b  <- doesFileExist pyBinPath
    b' <- doesDirectoryExist taggerDirPath
    case (b, b') of
      (_    , False) -> error "Tagger directory not found"
      (False, _    ) -> error "Python bin not found"
      _              -> return ()
  --
  readWords :: FilePath -> IO [Text]
  readWords path = do
    exists <- doesFileExist path
    if exists
      then T.lines <$> TIO.readFile path
      else error $ "file `" <> path <> "` doesn't exist"

application :: Analyzer -> MVar ServerState -> WS.ServerApp
application analyzer state pending = do
  conn <- WS.acceptRequest pending
  WS.withPingThread conn 30 (return ()) $ do
    msg     <- WS.receiveData conn
    clients <- readMVar state
    let badNicknameRes :: Text
        badNicknameRes =
          "Name cannot contain punctuation or whitespace, and cannot be empty"
    case msg of
      _
        | any ($ fst client) [T.null, T.any isPunctuation, T.any isSpace]
        -> WS.sendTextData conn badNicknameRes
        | clientExists client clients
        -> WS.sendTextData conn ("User already exists" :: Text)
        | otherwise
        -> flip finally disconnect $ do
          modifyMVar_ state $ \s -> do
            let s' = addClient client s
            broadcast (fst client <> " joined") s'
            return s'
          talk analyzer client state
       where
        client     = (msg, conn)
        disconnect = do
          s <- modifyMVar state
            $ \s -> let s' = removeClient client s in return (s', s')
          broadcast (fst client <> " disconnected") s

talk :: Analyzer -> Client -> MVar ServerState -> IO ()
talk analyzer (user, conn) state = forever $ do
  msg       <- WS.receiveData conn
  editedMsg <- analyzer msg
  readMVar state >>= broadcast (user <> ": " <> editedMsg)

newServerState :: ServerState
newServerState = []

numClients :: ServerState -> Int
numClients = length

clientExists :: Client -> ServerState -> Bool
clientExists client = any ((== fst client) . fst)

addClient :: Client -> ServerState -> ServerState
addClient client clients = client : clients

removeClient :: Client -> ServerState -> ServerState
removeClient client = filter ((/= fst client) . fst)

broadcast :: Text -> ServerState -> IO ()
broadcast message clients = do
  TIO.putStrLn message
  forM_ clients $ \(_, conn) -> WS.sendTextData conn message
