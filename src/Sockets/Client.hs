{-# LANGUAGE OverloadedStrings #-}

module Sockets.Client where

import           Control.Concurrent             ( forkIO
                                                , killThread
                                                )
import           Control.Monad                  ( forever
                                                , unless
                                                , when
                                                )
import           Control.Monad.Trans            ( liftIO )
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import qualified Data.Text.IO                  as TIO
import           Data.Word                      ( Word16 )
import           Network.Socket                 ( withSocketsDo )
import qualified Network.WebSockets            as WS
import           System.Exit                    ( exitSuccess )


data ClientOpts = ClientOpts
  { serverAddr :: String
  , serverPort :: Word16
  }

runClient :: ClientOpts -> IO ()
runClient opts = withSocketsDo
  $ WS.runClient (serverAddr opts) (fromIntegral $ serverPort opts) "/" app

app :: WS.ClientApp ()
app conn = do
  putStrLn "Connected!"
  liftIO $ TIO.putStrLn "Please, enter your nickname:"
  nickname <- TIO.getLine
  WS.sendTextData conn nickname
  liftIO $ TIO.putStrLn "Your can exit the chat by entering **quit**"
  tId <- forkIO $ forever $ receiver conn
  let sender = do
        line <- TIO.getLine
        when (line == "**quit**") $ do
          WS.sendClose conn ("Farewell!" :: Text)
          killThread tId
          exitSuccess
        unless (T.null line) $ WS.sendTextData conn line
  forever $ sender

receiver :: WS.Connection -> IO ()
receiver conn = do
  msg <- WS.receiveData conn
  liftIO $ TIO.putStrLn msg
