{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric              #-}



module Nlp.Tagger
  ( execTagger
  , Pos(..)
  , Dependency(..)
  , Token(..)
  , ExecError(errDescription)
  ) where

import           Control.Exception              ( SomeException )
import           Data.Aeson                     ( FromJSON(..)
                                                , decode
                                                , withText
                                                )
import           Data.ByteString.Lazy           ( fromStrict )
import qualified Data.Text                     as T
import           Data.Text                      ( Text )
import           Data.Text.Encoding             ( encodeUtf8 )
import           GHC.Generics
import           Shelly                         ( Sh
                                                , handleany_sh
                                                , lastStderr
                                                , run
                                                , shelly
                                                , silently
                                                )


data ExecError = ExecError
  { errDescription :: Text
  }
  deriving (Show, Eq)

data Pos = Noun | Pronoun | Adjective | Verb | Auxiliary | OtherPos
    deriving (Show, Eq)

instance FromJSON Pos where
  parseJSON v = withText "Pos" (\txt -> pure $ textToPos txt) v

data Dependency = NominalSubject | Copula | Object | Root | AdjectivalModifier | OtherDep
  deriving (Show, Eq)

instance FromJSON Dependency where
  parseJSON v = withText "Dependency" (\txt -> pure $ textToDep txt) v

data Token = Token
  { word       :: Text
  , lemma      :: Text
  , lexeme     :: Text
  , pos        :: Pos
  , isPunct    :: Bool
  , dependency :: Dependency
  }
  deriving (Show, Eq, Generic)

instance FromJSON Token


execTagger :: FilePath -> FilePath -> Text -> IO (Either ExecError [Token])
execTagger pythonBin taggerPath inputText = do
  eitherOut <- execCmd
  case eitherOut of
    Left  err -> return $ Left err
    Right out -> do
      let err = ExecError "Couldn't parse tagger output"
      return $ maybe (Left err) Right (decode . fromStrict . encodeUtf8 $ out)
 where
  execCmd = shelly $ handleany_sh onSolverError $ runCmd
    pythonBin
    ([T.pack taggerPath, "-r", inputText])
  --
  runCmd cmd args = Right <$> (silently $ run cmd args)
  --
  onSolverError :: SomeException -> Sh (Either ExecError Text)
  onSolverError _ = do
    err <- lastStderr
    return $ Left $ ExecError err

-- Helpers
textToPos :: Text -> Pos
textToPos "NOUN" = Noun
textToPos "PRON" = Pronoun
textToPos "VERB" = Verb
textToPos "ADJ"  = Adjective
textToPos "AUX"  = Auxiliary
textToPos _      = OtherPos

textToDep :: Text -> Dependency
textToDep "nsubj" = NominalSubject
textToDep "cop"   = Copula
textToDep "obj"   = Object
textToDep "ROOT"  = Root
textToDep "amod"  = AdjectivalModifier
textToDep _       = OtherDep
