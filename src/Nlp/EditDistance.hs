module Nlp.EditDistance
  ( minEditCost
  , minEditSequence
  ) where

import           Data.Text                      ( Text )
import qualified Data.Text                     as T


-- Note: The tabulation scheme and structure of this algorithm was extracted from
-- the book "Algorithm Design With Haskell" by Richard Bird and Jeremy Gibbon, University
-- of Oxford.
-- My particular contributions to this algorithm are:
-- 1) Make it a bit more efficient by accumulating the cost in a tuple (Int, [EditOp]).
-- 2) Make it work with Text instead of String.
-- 3) Remove Substitution operation and only work with insertions and deletions.

data EditOp = Copy Char | Delete Char | Insert Char
  deriving (Show, Eq)

minEditCost :: Text -> Text -> Int
minEditCost t1 t2 = fst $ minEditSequence t1 t2

minEditSequence :: Text -> Text -> (Int, [EditOp])
minEditSequence xs ys = head (T.foldr (nextrow xs) (firstrow xs) ys)

firstrow :: Text -> [(Int, [EditOp])]
firstrow xs = T.foldr nextentry [(0, [])] xs
  where nextentry x row = cons (Delete x) (head row) : row

nextrow :: Text -> Char -> [(Int, [EditOp])] -> [(Int, [EditOp])]
nextrow ts y row = foldr step [cons (Insert y) (last row)] xes
 where
  xes :: [(Char, (Int, [EditOp]), (Int, [EditOp]))]
  xes = zip3Txt ts row (tail row)
  --
  step
    :: (Char, (Int, [EditOp]), (Int, [EditOp]))
    -> [(Int, [EditOp])]
    -> [(Int, [EditOp])]
  step (x, es1, es2) row' = if x == y
    then (cons (Copy x) es2) : row'
    else minWith fst [cons (Delete x) (head row'), cons (Insert y) es1] : row'

minWith :: Ord b => (a -> b) -> [a] -> a
minWith f = foldr1 smaller where smaller x y = if f x <= f y then x else y

zip3Txt :: Text -> [a] -> [b] -> [(Char, a, b)]
zip3Txt txt ls1 ls2
  | T.null txt
  = []
  | null ls1
  = []
  | null ls2
  = []
  | otherwise
  = let x = (T.head txt, head ls1, head ls2)
    in  x : zip3Txt (T.tail txt) (tail ls1) (tail ls2)

cons :: EditOp -> (Int, [EditOp]) -> (Int, [EditOp])
cons op (k, es) = (opCost op + k, op : es)
 where
  opCost :: EditOp -> Int
  opCost (Copy   _) = 0
  opCost (Delete _) = 1
  opCost (Insert _) = 1
