{-# LANGUAGE OverloadedStrings #-}

module Nlp.Analyzer
  ( mkAnalyzer
  , Analyzer
  ) where



import           Control.Applicative            ( (<|>) )
import           Control.Monad.State.Lazy       ( State
                                                , get
                                                , modify
                                                , runState
                                                )
import           Control.Monad.Writer.Lazy      ( WriterT
                                                , runWriterT
                                                , tell
                                                )
import           Data.IntMap                    ( IntMap )
import qualified Data.IntMap                   as M
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Data.Text.ICU.Char             ( Bool_(Diacritic)
                                                , property
                                                )
import           Data.Text.ICU.Normalize        ( NormalizationMode(NFD)
                                                , normalize
                                                )


import           Nlp.EditDistance               ( minEditCost )
import           Nlp.Tagger                     ( Dependency
                                                  ( AdjectivalModifier
                                                  , Copula
                                                  , NominalSubject
                                                  , Object
                                                  , Root
                                                  )
                                                , ExecError(errDescription)
                                                , Pos
                                                  ( Adjective
                                                  , Auxiliary
                                                  , Noun
                                                  , Pronoun
                                                  , Verb
                                                  )
                                                , Token
                                                  ( dependency
                                                  , isPunct
                                                  , lemma
                                                  , lexeme
                                                  , pos
                                                  , word
                                                  )
                                                , execTagger
                                                )
import           Nlp.Trie                       ( Trie
                                                , fromList
                                                , isWordMember
                                                )


type WS = WriterT Text (State (IntMap Text))
type Analyzer = Text -> IO Text

data WordKind = ProhibitedWord | BadWord | ContextualWord (Int, Token)
        deriving Show

newtype ProhibitedDict = ProhibitedDict { getWords :: [Text] }

mkAnalyzer :: FilePath -> FilePath -> [Text] -> [Text] -> [Text] -> Analyzer
mkAnalyzer pythonBin taggerPath pWords bWords cWords txt = do
  let prohibitedWords = ProhibitedDict $ adjustPwordDict pWords
      -- The original prohibited words are nevertheless added to the bad-words dict
      -- since their analysis is much cheaper.
      badWords        = fromList $ pWords <> bWords
      contextualWords = fromList cWords
  eitherTokens <- execTagger pythonBin taggerPath txt
  case eitherTokens of
    Right tkns -> do
      let result =
            (runState $ runWriterT $ analyzeWords prohibitedWords
                                                  badWords
                                                  contextualWords
                                                  tkns
              )
              M.empty
      return $ (snd . fst) result
    Left e -> error $ T.unpack $ errDescription e
 where
  -- As the prohibited-word analysis is so expensive, this dictionary is limited to only 20 words.
  -- Also as the longest word in spanish is `electroencefalografistas`, any words longer than 24 characters are discarded.
  -- Finally, the minimum word length for this dictionary is 8 because smaller words tend to match ones that are not bad at all.
  maxPWords    = 25
  --
  maxLenPwords = 24
  --
  minLenPwords = 8
  --
  pWordsFilter w =
    let len = T.length w in len >= minLenPwords && len <= maxLenPwords
  --
  adjustPwordDict :: [Text] -> [Text]
  adjustPwordDict = take maxPWords . filter pWordsFilter

analyzeWords :: ProhibitedDict -> Trie -> Trie -> [Token] -> WS ()
analyzeWords prohibitedWords badWords contextualWords nextTkns = go
  (zip [0 ..] nextTkns)
 where
  censorWord :: Text -> Text
  censorWord = T.map (const '*')
  --
  go :: [(Int, Token)] -> WS ()
  go []                = return ()
  go ((i, tkn) : tkns) = do
    s <- get
    let spaceIfNot b = if i == 0 || b then pure () else tell " "
    case M.lookup i s of
      -- If the word was already forward-analyzed no more analysis is needed:
      -- just censor it
      Just w -> do
        spaceIfNot $ isPunct tkn
        tell (censorWord w)
        go tkns
      Nothing -> do
        let maybeWordKind =
              analyzeToken prohibitedWords badWords contextualWords tkn tkns
        case maybeWordKind of
          -- If analysis returns Nothing, write word as is
          Nothing -> do
            spaceIfNot $ isPunct tkn
            tell (word tkn)
            go tkns
          -- If analysis returns a prohibited word, censor it
          (Just ProhibitedWord) -> do
            spaceIfNot $ isPunct tkn
            tell (censorWord . word $ tkn)
            go tkns
          -- If analysis returns a bad word, censor it
          (Just BadWord) -> do
            spaceIfNot $ isPunct tkn
            tell (censorWord . word $ tkn)
            go tkns
          -- If analysis returns a contextual word, censor the current token
          -- and insert the complement to the state
          (Just (ContextualWord (complementIndex, complementTkn))) -> do
            modify $ M.insert complementIndex (word complementTkn)
            spaceIfNot $ isPunct tkn
            tell (censorWord . word $ tkn)
            go tkns

analyzeToken
  :: ProhibitedDict -> Trie -> Trie -> Token -> [(Int, Token)] -> Maybe WordKind
analyzeToken prohibitedWords badWords contextualWords tkn nextTkns =
  -- Note that the `<|>` operator short-circuits, that is, if one alternative is successful
  -- it stops processing.
  prohibitedWordAnalysis <|> badWordAnalysis <|> contextAnalysis
 where
  -- This is the most expensive analysis as it might run `minEditCost` over
  -- all the words in the prohibited-dictionary--it might be that the whole
  -- dictionary is not consumed due to Haskell's laziness. That is why the
  -- prohibited-dictionary must be a tiny one of really bad words that must be
  -- censored.
  -- Note that this analysis is the most sensitive to misspellings of words.
  -- This analysis is done against the word as is.
  tolerance = 3
  --
  prohibitedWordAnalysis :: Maybe WordKind
  prohibitedWordAnalysis
    | null $ getWords prohibitedWords
    = Nothing
    | otherwise
    = let word' = T.toLower $ removeAccents $ word tkn
          costs = minEditCost word' <$> getWords prohibitedWords
      in  if any (<= tolerance) costs then Just ProhibitedWord else Nothing
  --
  -- This analysis is cheap and here we can take into account really huge dictionaries of
  -- thousands of words.
  -- Note that this analysis is not very sensitive to misspellings of words.
  -- This analysis is done against the lemma.
  badWordAnalysis :: Maybe WordKind
  badWordAnalysis = do
    let lemma'  = removeAccents $ lemma tkn
        lexeme' = removeAccents $ lexeme tkn
    if lemma' `isWordMember` badWords || lexeme' `isWordMember` badWords
      then Just BadWord
      else Nothing
  --
  -- This analysis is the last one to be applied if the first two haven't detected anything.
  -- The words to take into account here depend on the context of the sentence, that is,
  -- how the current token related gammatically to the following words.
  -- This analysis' cost depends on the depth of the context that we wan't to consider.
  -- This analysis is done against the lemma.
  contextAnalysis :: Maybe WordKind
  contextAnalysis = do
    (i, tkn') <- analyzeForwardContext
    if isContextualWord tkn && isContextualWord tkn'
      then Just $ ContextualWord (i, tkn')
      else Nothing
  --
  isContextualWord :: Token -> Bool
  isContextualWord tkn' =
    (removeAccents $ lemma tkn') `isWordMember` contextualWords
  --
  analyzeForwardContext :: Maybe (Int, Token)
  analyzeForwardContext = case (pos tkn, dependency tkn) of
    (Noun, Root) -> nounAdjectiveRule nextTkns
    (Noun, NominalSubject) ->
      nounAdjectiveRule nextTkns <|> nounCopulaRule nextTkns
    (Adjective, AdjectivalModifier) -> adjectiveNounRule nextTkns
    (Verb     , Root              ) -> verbRule nextTkns
    _                               -> Nothing


-- Helpers
-- Detect is a noun is being modified by an adjective
nounAdjectiveRule :: [(Int, Token)] -> Maybe (Int, Token)
nounAdjectiveRule tkns = do
  let contextDepth = 3
      context      = take contextDepth tkns
  (i, tkn, _) <- findWord isModifyingAdjective context
  return (i, tkn)

-- Detect is an adjective is modifying a noun that follows it
adjectiveNounRule :: [(Int, Token)] -> Maybe (Int, Token)
adjectiveNounRule tkns = do
  let contextDepth = 3
      context      = take contextDepth tkns
  (i, tkn, _) <- findWord isNoun context
  return (i, tkn)

-- Detect if a root verb is followed by its object
verbRule :: [(Int, Token)] -> Maybe (Int, Token)
verbRule tkns = do
  let contextDepth = 6
      context      = take contextDepth tkns
  (i, tkn, _) <- findWord isObjectOfVerb context
  return (i, tkn)

-- Detect if a noun or pronoun is followed by a copula and an adjective
nounCopulaRule :: [(Int, Token)] -> Maybe (Int, Token)
nounCopulaRule tkns = do
  let contextDepth = 6
      context      = take contextDepth tkns
  (_, _  , rest) <- findWord isAuxiliaryAndCopula context
  (j, tkn, _'  ) <- findWord isAdjective rest
  return (j, tkn)

findWord
  :: (Token -> Bool) -> [(Int, Token)] -> Maybe (Int, Token, [(Int, Token)])
findWord _ [] = Nothing
findWord p ((i, tkn) : tkns) =
  if p tkn then Just (i, tkn, tkns) else findWord p tkns

isAdjective :: Token -> Bool
isAdjective tkn = pos tkn == Adjective

isAuxiliaryAndCopula :: Token -> Bool
isAuxiliaryAndCopula tkn = case (pos tkn, dependency tkn) of
  (Auxiliary, Copula) -> True
  _                   -> False

isObjectOfVerb :: Token -> Bool
isObjectOfVerb tkn = case (pos tkn, dependency tkn) of
  (Noun   , Object) -> True
  (Pronoun, Object) -> True
  _                 -> False

isNoun :: Token -> Bool
isNoun tkn = pos tkn == Noun

isModifyingAdjective :: Token -> Bool
isModifyingAdjective tkn = case (pos tkn, dependency tkn) of
  (Adjective, AdjectivalModifier) -> True
  _                               -> False

removeAccents :: Text -> Text
removeAccents s = noAccents
 where
  noAccents      = T.filter (not . property Diacritic) normalizedText
  normalizedText = normalize NFD s
