module Nlp.Trie
  ( empty
  , insert
  , fromList
  , isWordMember
  , isPrefixMember
  , Trie
  ) where

import           Data.HashMap.Lazy              ( HashMap )
import qualified Data.HashMap.Lazy             as M
import           Data.Maybe                     ( isJust )
import qualified Data.Text                     as T
import           Data.Text                      ( Text )


type Children = HashMap Char Trie

data Trie = Trie
  { isWord   :: Bool
  , children :: Children
  }
  deriving Show

-- Empty Trie
empty :: Trie
empty = Trie False M.empty

-- Creation
fromList :: [Text] -> Trie
fromList = foldr insert empty

-- Insertion
insert :: Text -> Trie -> Trie
insert word t | T.null word = t { isWord = True }
              | otherwise   = Trie False (updateChildren word (children t))

updateChildren :: Text -> Children -> Children
updateChildren word chil
  | T.null word
  = chil
  | otherwise
  = let c    = T.head word
        rest = T.tail word
    in  case M.lookup c chil of
          Just t  -> M.insert c (insert rest t) chil
          Nothing -> M.insert c (insert rest empty) chil

-- Search
isWordMember :: Text -> Trie -> Bool
isWordMember word t = case getNode word t of
  Just t' -> isWord t'
  Nothing -> False

isPrefixMember :: Text -> Trie -> Bool
isPrefixMember prefix = isJust . getNode prefix

getNode :: Text -> Trie -> Maybe Trie
getNode word t
  | T.null word = Just t
  | otherwise = case M.lookup (T.head word) (children t) of
    Just t' -> getNode (T.tail word) t'
    Nothing -> Nothing
