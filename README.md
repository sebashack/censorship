# censorship

This application has been executed only on ubuntu 20.04

## First time setup

Run

```
./first-time-install.sh
```

## Build project

```
make build
```

## Exec chatserver

Run

```
stack exec chatserver -- -c <path-to-config-file>
```

## Exec chatserver

Run

```
stack exec chatclient -- -a <server-address> -p <server-port>
```

## Config file

There is an example `server-config.yaml` in the root dir of this project. Update the
values according to the location of the files on your local machine:


```
port: <server-port>
host: <server-address>
taggerDir: <path-to-tagger-directory>
prohibitedWords: <path-to-prohibited-words-dir>
badWords: <path-to-bad-words-dir>
contextualWords: <path-to-contextual-words-dir>
```

`taggerDir` should always be the absolute path to `/pos-tagger` in this project unless
you want to use another compatible tagger.

There are already dictionary files in the `dictionaries` directory in this project, nevertheless
you can specify other absolute paths to your own files.
