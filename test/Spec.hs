import qualified EditDistanceSpec              as EditDistance
import           Test.Tasty                     ( defaultMain
                                                , testGroup
                                                )
import qualified TrieSpec                      as Trie


main :: IO ()
main = do
  trieSpecs    <- Trie.tests
  editDisSpecs <- EditDistance.tests
  defaultMain $ testGroup "Main" [trieSpecs, editDisSpecs]
