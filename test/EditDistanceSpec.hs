{-# LANGUAGE OverloadedStrings #-}

module EditDistanceSpec
  ( tests
  ) where

import           Test.Tasty                     ( TestTree
                                                , testGroup
                                                )
import           Test.Tasty.Hspec               ( Spec
                                                , describe
                                                , it
                                                , shouldBe
                                                , testSpecs
                                                )


import           Nlp.EditDistance               ( minEditCost )


tests :: IO TestTree
tests = do
  specs <- testSpecs $ editDisSpec
  return $ testGroup "Edit Distance Specs" specs

editDisSpec :: Spec
editDisSpec = describe "minEditCost" $ do
  it "computes the distance between two words" $ do
    minEditCost "motor" "motor" `shouldBe` 0
    minEditCost "motor" "moto" `shouldBe` 1
    minEditCost "motor" "motoe" `shouldBe` 2
    minEditCost "motorcbike" "motorcycle" `shouldBe` 6
    minEditCost "motorbike" "motorcycle" `shouldBe` 7
    minEditCost "sun*risee" "sunrise" `shouldBe` 2
    minEditCost "sun*riz" "sunrise" `shouldBe` 4
