{-# LANGUAGE OverloadedStrings #-}

module TrieSpec
  ( tests
  ) where

import           Test.Tasty                     ( TestTree
                                                , testGroup
                                                )
import           Test.Tasty.Hspec               ( Spec
                                                , describe
                                                , it
                                                , shouldBe
                                                , testSpecs
                                                )


import           Nlp.Trie                       ( fromList
                                                , isPrefixMember
                                                , isWordMember
                                                )


tests :: IO TestTree
tests = do
  specs <- testSpecs $ trieSpec
  return $ testGroup "Trie Specs" specs

trieSpec :: Spec
trieSpec = do
  describe "isWordMember" $ do
    it "finds a word" $ do
      let t = fromList
            [ "motor"
            , "bike"
            , "motorbike"
            , "train"
            , "train-wreck"
            , "grammar"
            , "grammy"
            ]
      isWordMember "motor" t `shouldBe` True
      isWordMember "motorbike" t `shouldBe` True
      isWordMember "grammar" t `shouldBe` True
      isWordMember "train" t `shouldBe` True
      isWordMember "train-wreck" t `shouldBe` True
    it "finds a word as prefix" $ do
      let t = fromList
            [ "motor"
            , "bike"
            , "motorbike"
            , "train"
            , "train-wreck"
            , "grammar"
            , "grammy"
            ]
      isPrefixMember "motor" t `shouldBe` True
      isPrefixMember "motorbike" t `shouldBe` True
      isPrefixMember "grammar" t `shouldBe` True
      isPrefixMember "train" t `shouldBe` True
      isPrefixMember "train-wreck" t `shouldBe` True
    it "finds a prefix when dictionary has only words" $ do
      let
        t =
          fromList ["motorbike", "train-wreck", "grammar", "grammy", "catfish"]
      isPrefixMember "motor" t `shouldBe` True
      isWordMember "motor" t `shouldBe` False
      isPrefixMember "train" t `shouldBe` True
      isWordMember "train" t `shouldBe` False
      isPrefixMember "gramm" t `shouldBe` True
      isWordMember "gramm" t `shouldBe` False
      isPrefixMember "cat" t `shouldBe` True
      isWordMember "cat" t `shouldBe` False
    it "finds a prefix when dictionary has prefixes and words" $ do
      let
        t = fromList
          ["motor", "motorbike", "train-wreck", "train", "cat", "catfish"]
      isPrefixMember "motor" t `shouldBe` True
      isPrefixMember "train" t `shouldBe` True
      isPrefixMember "cat" t `shouldBe` True
    it "finds words and prefixes with character `ñ`" $ do
      let t = fromList
            [ "ñaque"
            , "ñu"
            , "año"
            , "bañera"
            , "compañeros"
            , "cuñada"
            , "engaño"
            , "niñero"
            , "niño"
            ]
      isWordMember "ñaque" t `shouldBe` True
      isWordMember "ñu" t `shouldBe` True
      isWordMember "cuñada" t `shouldBe` True
      isPrefixMember "compa" t `shouldBe` True
      isPrefixMember "compañero" t `shouldBe` True
      isPrefixMember "niñ" t `shouldBe` True
